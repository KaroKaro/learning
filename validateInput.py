while True:
    print('Podaj swój wiek')
    age = input()
    if age.isdecimal():
        break
    print("Wiek musi być podany w liczbach")

while True:
    print('Wybierz nowe hasło (jedynie litery i cyfry):')
    password = input()
    if password.isalnum():
        break
    print('Hasło może się składać jedynie z liter i cyfr')