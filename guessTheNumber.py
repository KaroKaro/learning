import random
secretnumber = random.randint(1, 20)
print("Mam na myśli pewną liczbę z zakresu od 1 do 20")

for guesstaken in range (1, 7):
    print("Spróbuj odgadnąć liczbę")
    guess = int(input())

    if guess < secretnumber:
        print("Podana liczba jest za mała")
    elif guess > secretnumber:
        print("Podana liczba jest za duża")
    else:
        break

if guess == secretnumber:
    print("Super! Odgadłeś liczbę w ciągu " + str(guesstaken) + " prób!")
else:
    print("Nie udało ci się. Liczba, którą miałem na myśli to " + str(secretnumber))